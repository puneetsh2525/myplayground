// Simple Linear Search in Swift

import Foundation

// Linear Search
func linearSearch<T:Equatable>(_ array:[T],_ object:T) -> Int? {
    for (index, element) in array.enumerated() where element == object {
        return index
    }
    return nil
}

linearSearch([1, 4, 7], 4)

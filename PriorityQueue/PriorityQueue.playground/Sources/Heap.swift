import Foundation

enum HeapError:Error {
    case underflow // throw this error when heap underflows
    case indexNotFound
}

public struct Heap<T>:CustomStringConvertible {
    private var elements:[T]!
    private let priority:(T,T) -> Bool
    
    public var size:Int {
        return elements.count
    }
    
    public var description:String {
        return elements.description
    }
    
    public init(priority:@escaping (T, T)-> Bool) {
        self.priority = priority
        self.elements = [T]()
    }
    
    // MARK: Public API
    public mutating func buildHeap(fromArray array:[T]) {
        elements = array
        let midIndex = size/2 - 1
        for i in (0...midIndex).reversed() {
            heapifyAt(i, heapSize:elements.count)
        }
    }
    
    public func top() throws -> T {
        guard let first = elements.first else {
            throw HeapError.underflow
        }
        return first
    }
    
    public mutating func pop() throws -> T  {
        guard let first = elements.first else {
            throw HeapError.underflow
        }
        if let last = elements.last {
            elements[0] = last
            let heapSize = size - 1
            heapifyAt(0, heapSize: heapSize)
        }
        return first
    }
    
    public mutating func replaceElement(_ element:T, atIndex i:Int) throws {
        guard i < size else {
            throw HeapError.indexNotFound
        }
        
        elements[i] = element
        checkParentAt(i)
    }
    
    public mutating func insertElement(element:T) {
        elements.append(element)
        checkParentAt(size-1)
    }
    
    // MARK: Private Methods
    private mutating func heapifyAt(_ i:Int, heapSize:Int) {
        let left = leftOf(i)
        let right = rightOf(i)
        var largest = i
        
        if left < heapSize && priority(elements[left], elements[largest]) {
            largest = left
        }
        
        if  right < heapSize && priority(elements[right], elements[largest]) {
            largest = right
        }
        
        if largest != i {
            elements.swapAt(i, largest)
            heapifyAt(largest, heapSize:heapSize)
        }
    }
    
    private mutating func checkParentAt(_ i:Int)  {
        guard i > 0 && i < size else {
            return
        }
        
        let parent = parentOf(i)
        guard priority(elements[i], elements[parent]) else {
            return
        }
        
        elements.swapAt(i, parent)
        checkParentAt(parent)
    }
    
    private func leftOf(_ i:Int) -> Int {
        return 2*i
    }
    
    private func rightOf(_ i:Int) -> Int {
        return leftOf(i) + 1
    }
    
    private func parentOf(_ i:Int) -> Int {
        return i/2
    }
}

extension Heap {
    public static func sort(array:[T], priority: @escaping (T, T) -> Bool) -> [T] {
        var heap:Heap<T> = Heap<T>(priority: priority)
        heap.buildHeap(fromArray: array)
        var heapSize = heap.size
        for i in (1...heapSize-1).reversed() {
            heap.elements.swapAt(0, i)
            heapSize = heapSize-1
            heap.heapifyAt(0, heapSize: heapSize)
        }
        return heap.elements
    }
}


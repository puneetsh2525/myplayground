import Foundation

/// Max-Heap
// Building a MAx-Heap
var maxHeap = Heap<Int>(priority:>)
maxHeap.buildHeap(fromArray: [1, 7, 2, 3, 9, 16])

// Testing pop
try maxHeap.pop()
// Testing top
try maxHeap.top()
maxHeap

// Testing Insert
try maxHeap.insertElement(element: 10)
maxHeap

// Testing replace
try maxHeap.replaceElement(20, atIndex: 6)
maxHeap


/// Min-Heap
// Building a MAx-Heap
var minHeap = Heap<Int>(priority:<)
minHeap.buildHeap(fromArray: [1, 7, 2, 3, 9, 16])

// Testing pop
try minHeap.pop()
// Testing top
try minHeap.top()
minHeap

// Testing Insert
try minHeap.insertElement(element: 2)
minHeap

// Testing replace
try minHeap.replaceElement(1, atIndex: 6)
minHeap


/// Sort
Heap.sort(array: [2, 9, 1, 3], priority: >)

import Foundation

public class TreeNode<Key:Comparable, Payload> {
    public typealias Node = TreeNode<Key, Payload>
    public var leftChild:Node?
    public var rightChild:Node?
    public var parent:Node?
    public var key:Key
    public var payload:Payload?
    public var height:Int
    
    public init(key: Key, payload: Payload?, leftChild: Node?, rightChild: Node?, parent: Node?, height: Int) {
        self.key = key
        self.payload = payload
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.parent = parent
        self.height = height
        
        self.leftChild?.parent = self
        self.rightChild?.parent = self
    }

}

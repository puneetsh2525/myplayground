/*
 Boyer-Moore String Search - https://github.com/raywenderlich/swift-algorithm-club/tree/master/Boyer-Moore#boyer-moore-string-search
 
 Goal: Write a string search algorithm in pure Swift without importing Foundation or using NSString's rangeOfString() method.
 
 In other words, we want to implement an indexOf(pattern: String) extension on String that returns the String.Index of the first occurrence of the search pattern, or nil if the pattern could not be found inside the string.
 
 // Input:
 let s = "Hello, World"
 s.indexOf(pattern: "World")
 
 // Output:
 <String.Index?> 7
 
 */

import Foundation

// Brute-Force Solution
extension String {
    func bruteIndexOf(pattern:String) -> Index? {
        guard pattern.count > 0 else {
            return nil
        }
        
        var startIndex:Index?
        var j = self.startIndex
        for i in self.indices {
            j = j > i ? j : i
            var found = true
            for p in pattern.indices {
                if j == self.endIndex || pattern[p] != self[j] {
                    found = false
                    break
                } else {
                    j = self.index(after: j)
                }
            }
            if found {
                startIndex = i
                break
            }
        }
        
        return startIndex
    }
}

let s = "Hello, World"
let bruteIndex = s.bruteIndexOf(pattern: "llo")
print(bruteIndex?.encodedOffset ?? "not found")

// Boyer-Moore
extension String {
    func boyerMooreIndexOf(pattern:String) -> Index? {
        let patternLength = pattern.count
        guard patternLength > 0 && self.count > patternLength else {
            return nil
        }
        
        var skipTable:[Character:Int] = [:]
        for (index, c) in pattern.enumerated() {
            skipTable[c] = patternLength - index - 1
        }
    
        let p = pattern.index(before: pattern.endIndex)
        let lastChar = pattern[p]
        var i = self.index(startIndex, offsetBy: patternLength-1)
        
        func backwards() -> Index? {
            var j = i
            var q = p
            while q > pattern.startIndex {
                j = index(before: j)
                q = index(before: q)
                if pattern[q] != self[j] {
                    return nil
                }
            }
            return j
        }
        
        while i < endIndex {
            let c = self[i]
            if c == lastChar {
                if let b =  backwards() {
                    return b
                }
                i = index(after: i)
            } else {
                i = index(i, offsetBy: skipTable[c] ?? patternLength, limitedBy: endIndex) ?? endIndex
            }
        }
        return nil
    }
}

let a = "abdeddddrr"
let index = a.boyerMooreIndexOf(pattern: "edd")
print(index?.encodedOffset ?? "not found")

import Foundation

public struct WeightedUnionFind {
    private var ids:[Int]!
    private var size:[Int]!
    let nodes:Int
    
    public init(nodes:Int) {
        self.nodes = nodes
        ids = Array(0..<nodes)
        size = Array(0..<nodes)
    }
    
    private mutating func root(for i:Int) -> Int {
        var index = i;
        while index != ids[index] {
            ids[index] = ids[ids[index]] // path compression
            index = ids[index]
        }
        return index
    }
    
    public mutating func isNodesConnected(p:Int, q:Int)->Bool {
        return root(for:p) == root(for:q)
    }
    
    public mutating func union(p:Int, q:Int) {
        let rootp = root(for:p)
        let rootq = root(for:q)
        
        guard rootp != rootq else {
            return
        }
        
        if size[p] < size[q] {
            ids[rootp] = rootq
            size[p] += size[q]
        } else {
            ids[rootq] = rootp
            size[q] += size[p]
        }
    }
}

import Foundation
import UIKit

var unionFind:WeightedUnionFind = WeightedUnionFind(nodes: 10)

let image = #imageLiteral(resourceName: "UnionFind.png")

unionFind.union(p: 0, q: 5)
unionFind.union(p: 5, q: 6)
unionFind.union(p: 6, q: 1)
unionFind.union(p: 1, q: 2)
unionFind.union(p: 2, q: 7)
unionFind.union(p: 3, q: 8)
unionFind.union(p: 3, q: 4)
unionFind.union(p: 4, q: 9)

unionFind.isNodesConnected(p: 0, q: 7)
unionFind.isNodesConnected(p: 2, q: 9)
unionFind.isNodesConnected(p: 8, q: 4)

// Goal: To implement custom operator in Swift
// Inspiration: This talk by Erica Sadun: https://realm.io/news/slug-erica-sadun-operators-strong-opinions/?utm_campaign=This%2BWeek%2Bin%2BSwift&utm_medium=web&utm_source=This_Week_in_Swift_120

// Implement postfix operator which Apple deprecated in Swift 3

import Foundation

postfix operator ++
prefix operator ++

extension  Int {
    static postfix func ++(int: inout Int) -> Int {
        int =  int+1
        return int
    }
    
    static prefix func ++(int: inout Int) -> Int {
        int = int+1
        return int
    }
}

var i = 9
var j = i++
j
++i

// Implement Infix Operator

precedencegroup UnionPrecedence { // Updated for Swift 4
    associativity: left
    higherThan: AdditionPrecedence
}

infix operator ⊎ : UnionPrecedence

extension Set {
    static func ⊎(set1:Set, set2:Set) -> Set {
        return set1.union(set2)
    }
}

let s1:Set = [1, 2, 3]
let s2:Set = [2, 3]
let s3:Set = s1 ⊎ s2


import Foundation

class LinkedListNode<T:Comparable> {
    var prev:LinkedListNode<T>?
    var next:LinkedListNode<T>?
    let value:T
    init(value:T) {
        self.value = value
    }
}

public class BoundedPriorityQueue<T:Comparable> {
    typealias Node = LinkedListNode<T>
    
    private let maxElements:Int
    public private(set) var count:Int = 0
    private var head:Node?
    private var tail:Node?
    
    public init(maxElements:Int) {
        self.maxElements = maxElements
    }
}

extension BoundedPriorityQueue {
    public func peek() -> T? {
        return head?.value
    }
    
    public var isEmpty:Bool {
        return count == 0
    }
    
    public func enqueue(_ value:T) {
        if let node = insert(value, after: findInsertionPointFor(value)) {
            if node.next == nil {
                tail = node
            }
            count += 1
            if count > maxElements {
                removeLastImportantElement()
            }
        }
    }
    
    public func dequeue() ->T? {
        if let first = head {
            count -= 1
            if count == 0 {
                head = nil
                tail = nil
            } else {
                head = first.next
                head?.prev = nil
            }
            return first.value
        } else {
            return nil
        }
    }
}

extension BoundedPriorityQueue {
    private func findInsertionPointFor(_ value:T) -> Node? {
        var node = head
        var prev:Node?
        while let current = node, current.value > value {
            prev = node
            node = node?.next
        }
        return prev
    }
    
    private func insert(_ value:T, after:Node?) -> Node? {
        if let previous = after {
            if count == maxElements && previous.next == nil {
                return nil
            }
            let node = Node(value: value)
            node.next = previous.next
            node.prev = previous
            previous.next = node
            node.next?.prev = node
            return node
        } else if let first = head {
            head = Node.init(value: value)
            head!.next = first
            first.prev = head
            return head
        } else {
            head = Node.init(value: value)
            return head
        }
    }
    
    private func removeLastImportantElement() {
        if let last = tail {
            let prev = last.prev
            prev?.next = nil
            tail = prev
            count -= 1
        }
    }
}

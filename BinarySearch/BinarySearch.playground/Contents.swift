// Simple binary search

import Foundation

func binarySearch<T:Comparable>(_ array:Array<T>, object:T, range:Range<Int>) -> Int? {
    guard range.lowerBound < range.upperBound else {
        return nil
    }
    let midIndex = range.lowerBound + (range.upperBound - range.lowerBound)/2
    let mid = array[midIndex]
    if mid == object {
        return midIndex
    } else if mid > object {
        return binarySearch(array, object: object, range: range.lowerBound..<midIndex)
    } else {
        return binarySearch(array, object: object, range: midIndex+1..<range.upperBound)
    }
}

binarySearch([1, 3, 4, 6], object: 1, range: 0..<4)

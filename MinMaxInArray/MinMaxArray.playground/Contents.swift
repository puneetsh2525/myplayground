// Goal: Find the minimum/maximum object in an unsorted array.

import Foundation
extension Array where Element:Comparable {
    func minMaxPair() -> (min: Element?, max: Element?) {
        guard !self.isEmpty else {
            return (nil, nil)
        }
        
        var currentMin = self.first!
        var currentMax = self.first!
        
        for value in self {
            if currentMin > value {
                currentMin = value
            } else  if currentMax < value {
                currentMax = value
            }
        }
        
        return (currentMin, currentMax)
    }
}

extension Array {
    func minMaxPair(by minMax:(Element, Element, Element) throws ->(min:Element, max:Element)) rethrows -> (Element?, Element?) {
        guard !self.isEmpty else {
            return (nil, nil)
        }
        
        var currentMin = self.first!
        var currentMax = self.first!
        
        for value in self {
            let elements = try minMax(currentMin, currentMax, value)
            currentMin = elements.min
            currentMax = elements.max
        }
        
        return (currentMin, currentMax)
    }
}


[1, 3, 2, 8, 5].minMaxPair { (cMin, cMax, v) -> (min: Int, max: Int) in
    if cMin > v {
        return (v, cMax)
    } else if cMax < v {
        return (cMin, v)
    }
    return (cMin, cMax)
}

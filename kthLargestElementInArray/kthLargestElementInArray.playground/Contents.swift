// Goal: You're given an integer array a. Write an algorithm that finds the k-th largest element in the array.
// https://github.com/raywenderlich/swift-algorithm-club/tree/master/Kth%20Largest%20Element


import Foundation

// Algo Hint: Mix of BinarySearch and QuickSort
func kthLargest(array:[Int], k:Int) -> Int {
    var a = array
    
    func random(min:Int, max:Int) -> Int {
        let upperBound:UInt32 = UInt32(max) - UInt32(min)
        return Int(arc4random_uniform(upperBound)) + min
    }
    
    func randomPivot(a:inout [Int], low:Int, high:Int) -> Int {
        let pivotIndex = random(min: low, max: high)
        if high != pivotIndex {
            a.swapAt(high, pivotIndex)
        }
        return a[high]
    }
    
    func randomizedPartition(a:inout [Int], low:Int, high:Int) -> Int {
        let pivot = randomPivot(a: &a, low: low, high: high)
        var i = low
        for j in low..<high where  a[j] <= pivot {
            if i != j {
                a.swapAt(i, j)
            }
            i += 1
        }
        if high != i {
            a.swapAt(high, i)
        }
        
        return i
    }
    
    func randomizedSelection(a:inout [Int], low:Int, high:Int, k:Int) -> Int {
        print("low:\(low), high:\(high)")
        guard low < high else {
            return a[low]
        }
        
        let p = randomizedPartition(a: &a, low: low, high: high)
        if k == p {
            return a[k]
        } else if k < p {
            return randomizedSelection(a: &a, low: low, high: p-1, k:k)
        } else {
            return randomizedSelection(a: &a, low: p+1, high: high, k:k)
        }
    }
    
    return randomizedSelection(a: &a , low: 0, high: a.count-1, k: a.count-k)
}


let array = [1, 7, 5, 15, 9]
kthLargest(array: array, k: 2)

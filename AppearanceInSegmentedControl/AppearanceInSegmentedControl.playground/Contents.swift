// I created this playground to solve a stackoverflow question: https://stackoverflow.com/questions/42062659/uiimageview-appearance-is-overriding-uisegmentedcontrol-appearance/42062988#42062988

import UIKit
import PlaygroundSupport

class MyView:UIView {
    override func draw(_ rect: CGRect) {
        UIImageView.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).tintColor = UIColor.red
        
        UIImageView.appearance().tintColor = UIColor.green
        
        let segmentedControl = UISegmentedControl(items: ["1", "2"])
        
        let imageView = UIImageView(frame: CGRect(x:0, y: rect.height/2, width: 150, height: 44))
        imageView.image = nil
        
        self.addSubview(segmentedControl)
        self.addSubview(imageView)
        self.backgroundColor = .white
    }
    
}

let v = MyView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
PlaygroundPage.current.needsIndefiniteExecution = true
PlaygroundPage.current.liveView = v

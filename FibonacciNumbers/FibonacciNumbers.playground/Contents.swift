//: Playground - noun: a place where people can play

import UIKit

import Foundation

// http://www.geeksforgeeks.org/count-ways-reach-nth-stair/
// Goal:

// This is too slow, exponential growth
func fibRecursion(_ num:Int) -> Int {
    guard num > 1 else {
        return num
    }
    return fibRecursion(num-1)+fibRecursion(num-2)
}

// Iterative
func fibIterative(_ num:Int64) -> Int64 {
    
    if(num == 0) {
        return 0
    } else if(num == 1||num == 2) {
        return 1
    }
    
    var current:Int64 = 1, prev:Int64 = 1
    for _ in 2..<num {
        let tempCurrent = current
        current = prev+current
        prev = tempCurrent
    }
    return current
}

fibIterative(40)



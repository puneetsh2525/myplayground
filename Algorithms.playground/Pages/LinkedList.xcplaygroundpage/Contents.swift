import Foundation
import UIKit

public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init(_ val: Int) {
        self.val = val
        self.next = nil
    }
    
   
}

class Solution {
    func reverseList(_ head: ListNode?) -> ListNode? {
        guard head != nil, head?.next != nil else { // check for empty or one value list
            return head
        }
        var prev:ListNode? = nil
        var current:ListNode? = head
        var next:ListNode?
        while current != nil {
            next = current?.next
            current?.next = prev
            prev = current
            current = next
        }
        return prev
    }
    
    func printHead(head:ListNode?) {
        var next = head
        while next != nil {
            print(next!.val)
            next = next?.next
        }
    }
}

let head = ListNode(1)
head.next = ListNode(2)
head.next!.next = ListNode(3)
head.next!.next!.next = ListNode(4)
head.next!.next!.next!.next = ListNode(5)

let s = Solution()
let r = s.reverseList(head)
s.printHead(head: r)

let view:UIVisualEffectView = UIVisualEffectView()
view.isUserInteractionEnabled = false

//: [Previous](@previous)

import Foundation

// Jump Game

class Solution {
    lazy var coinMap:[Int:Int] = {
        return [Int:Int]()
    }()
    
    func canJump(_ nums: [Int]) -> Bool {
        guard nums.count > 1 else {
            return true
        }
        var last = nums.count-1
        for i in stride(from: nums.count-2, to: -1, by: -1) {
            if nums[i]+i >= last {
                last = i
            }
        }
        return last <= 0
    }
}

// Unique Paths
extension Solution {
    func uniquePaths(_ m: Int, _ n: Int) -> Int {
        guard m > 1, n > 1 else {
            return 1
        }
        var map:Array<[Int]> = Array<[Int]>(repeating: [Int].init(repeating: 1, count: m), count: n)
        for i in 1..<n {
            for j in 1..<m {
                map[i][j] = map[i-1][j] + map[i][j-1]
            }
        }
        return map[n-1][m-1]
    }
}

// Coin change
//extension Solution {
//    func coinChange(_ coins: [Int], _ amount: Int) -> Int {
//
//    }
//}

//  Longest Increasing Subsequence

extension Solution {
    private func indexOf(_ num:Int, in pile:[Int]) -> Int? {
        var low = 0
        var high = pile.count-1
        while low <= high {
            let mid = low+(high-low)/2
            if pile[mid] == num {
                return mid
            } else if pile[mid] < num {
                low = mid+1
            } else {
                high = mid-1
            }
        }
        if high >= 0 && high < pile.count && pile[high] > num {
            return high
        } else if low >= 0 && low < pile.count && pile[low] > num {
            return low
        }
        return nil
    }
    
    func lengthOfLIS(_ nums: [Int]) -> Int {
        guard nums.count > 1 else {
            return nums.count
        }
        var pile:[Int] = [nums.first!]
        for i in stride(from: 1, to: nums.count, by: 1) {
            let num = nums[i]
            if let index = indexOf(num, in: pile) {
                pile[index] = num
            } else {
                pile.append(num)
            }
        }
        return pile.count
    }
}

let s = Solution()
s.lengthOfLIS([10,9,2,5,3,7,101,18])

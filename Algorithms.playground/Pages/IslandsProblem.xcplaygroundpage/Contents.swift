//: [Previous](@previous)

import Foundation


class Solution {
    
    func DFS(_ grid:inout [[Character]], _ x: Int, _ y: Int)
    {
        if grid[x][y] == "1" {
            grid[x][y] = "0"
            if x - 1 >= 0 && grid[x-1][y] == "1" {
                DFS(&grid, x-1, y)
            }
            if y - 1 >= 0 && grid[x][y-1] == "1" {
                DFS(&grid, x, y-1)
            }
            if x + 1 < grid.count && grid[x+1][y] == "1" {
                DFS(&grid, x+1, y)
            }
            if y + 1 < grid[0].count && grid[x][y+1] == "1" {
                DFS(&grid, x, y+1)
            }
        }
    }
    
    func numIslands(_ grid: [[Character]]) -> Int {
        var grid = grid
        if grid.isEmpty {
            return 0
        }
        var count = 0
        let row = grid.count, col = grid[0].count
        for i in 0..<row {
            for j in 0..<col {
                if grid[i][j] == "1" {
                    DFS(&grid, i, j)
                    count += 1
                }
            }
        }
        return count
    }
}

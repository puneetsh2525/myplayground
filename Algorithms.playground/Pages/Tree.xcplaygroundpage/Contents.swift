//: [Previous](@previous)

import Foundation

public class TreeNode {
         public var val: Int
         public var left: TreeNode?
         public var right: TreeNode?
         public init(_ val: Int) {
                 self.val = val
                    self.left = nil
                     self.right = nil
            }
}

// Level Order
class Solution {
    func levelOrder(_ root: TreeNode?) -> [[Int]] {
        var result:[[Int]] = [[Int]]()
        var nodes = [root]
        whileloop:while true {
            var array:[Int] = []
            var newNodes:[TreeNode?] = []
            for node in nodes {
                if let node = node {
                    array.append(node.val)
                    newNodes.append(node.left)
                    newNodes.append(node.right)
                }
            }
            nodes = newNodes
            if array.count > 0 {
                result.append(array)
            } else {
                break whileloop
            }
        }
        return result
    }
}

// BST from sorted Array
extension Solution {
    func sortedArrayToBST(_ nums: [Int]) -> TreeNode? {
        guard nums.count > 0 else {
            return nil
        }
        return merge(nums, left: 0, right:nums.count-1)
    }
    
    private func merge(_ nums:[Int], left:Int, right:Int) -> TreeNode? {
        let mid = left + (right-left)/2
        if left > right {
            return nil
        } else if left < right && (left == mid || right == mid) {
            let n = TreeNode.init(nums[right])
            n.left = TreeNode.init(nums[left])
            return n
        } else {
            let root = TreeNode.init(nums[mid])
            root.left = merge(nums, left: left, right: mid-1)
            root.right = merge(nums, left: mid+1, right: right)
            return root
        }
    }
}

// Inorder Traversal
extension Solution {
    func inorderTraversal(_ root: TreeNode?) -> [Int] {
        guard let root = root else {return []}
        var array:[Int] = []
        array.append(contentsOf: inorderTraversal(root.left))
        array.append(root.val)
        array.append(contentsOf: inorderTraversal(root.right))
        return array
    }
}

// Zigzag Order Traversal
extension Solution {
    func zigzagLevelOrder(_ root: TreeNode?) -> [[Int]] {
        var result:[[Int]] = [[Int]]()
        guard let root = root else {return result}
        
        var nodes:[TreeNode] = [root]
        var fromLeft:Bool = true
        whileloop:while true {
            var array:[Int] = []
            var newNodes:[TreeNode] = []
            let count = nodes.count
            for i in 0..<count {
                let newNode = nodes[i]
                let arrayNode = fromLeft ? nodes[i] : nodes[count-i-1]
                if let left = newNode.left {
                    newNodes.append(left)
                }
                if let right = newNode.right {
                    newNodes.append(right)
                }
                array.append(arrayNode.val)
            }
            fromLeft = !fromLeft
            nodes = newNodes
            if array.count > 0 {
                result.append(array)
            } else {
                break whileloop
            }
        }
        return result
    }
}

// Construct Binary Tree from Preorder and Inorder Traversal


extension Solution {
    
    func buildTreeHelper(_ preorder: [Int], _ inorder: [Int], inStart:Int, inEnd:Int, preIndex:Int, map:[Int:Int]) -> (TreeNode?, Int) {
        guard inStart <= inEnd else {return (nil, preIndex)}
        let node = TreeNode.init(preorder[preIndex])
        if inStart == inEnd {return (node, preIndex+1)}
        let inIndex = map[node.val]!
        node.left = buildTreeHelper(preorder, inorder, inStart: inStart, inEnd: inIndex-1, preIndex: preIndex+1, map: map).0
        node.right = buildTreeHelper(preorder, inorder, inStart: inIndex+1, inEnd: inEnd, preIndex: preIndex+1, map: map).0
        return (node, preIndex+1)
    }
    
    func buildTree(_ preorder: [Int], _ inorder: [Int]) -> TreeNode? {
        var map:[Int:Int] = [:]
        for (index, val) in inorder.enumerated() {
            map[val] = index
        }
        return buildTreeHelper(preorder, inorder, inStart: 0, inEnd: inorder.count-1, preIndex: 0, map: map).0
    }
}

// Kth Smallest Element in a BST

extension Solution {
    
    func inorderTraversalHelper(_ root: TreeNode?, k:Int, shouldContinue:Bool) -> [Int] {
        var array:[Int] = []
        guard let root = root, shouldContinue == true else {return array}
        var sContinue = shouldContinue
        let leftResult = inorderTraversalHelper(root.left, k:k, shouldContinue: sContinue)
        array.append(contentsOf: leftResult)
        array.append(root.val)
        sContinue = array.count < k
        let rightResult = inorderTraversalHelper(root.right, k:k, shouldContinue: sContinue)
        array.append(contentsOf: rightResult)
        return array
    }
    
    func kthSmallest(_ root: TreeNode?, _ k: Int) -> Int {
        let array = inorderTraversalHelper(root, k: k, shouldContinue: true)
        guard array.count >= k else {return 0}
        return array[k-1]
    }
}

func inorder(_ root:TreeNode?) {
    guard let n = root else {return}
    inorder(n.left)
    print(n.val)
    inorder(n.right)
}

let n1 = TreeNode(1)
let n2 = TreeNode(2)
let n3 = TreeNode(3)
let n4 = TreeNode(4)
let n5 = TreeNode(5)

n1.left = n2
n1.right = n3
n2.left = n4
n3.right = n5

let s = Solution().zigzagLevelOrder(n1)
print(s)

//: [Previous](@previous)

import Foundation

func twoSum(_ nums: [Int], target: Int) -> (Int, Int)? {
    var set = Set<Int>()
    nums.forEach { num in
        set.insert(target-num)
    }
    
    for num in nums {
        if set.contains(num) {
            return (num, target-num)
        }
    }
    return nil
}

twoSum([3, 2, 9, 8], target: 13)

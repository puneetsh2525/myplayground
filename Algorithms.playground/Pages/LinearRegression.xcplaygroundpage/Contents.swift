//: [Previous](@previous)

import Foundation

func calculateMean(_ array:[Double]) -> Double {
    let count = array.count
    guard count > 0 else {return 0}
    let sum = array.reduce(0, +)
    return sum/Double(count)
}

func calculateSumOfSquareOfMeanMinusValues(values:[Double], mean:Double) -> Double {
    guard values.count > 0 else {return 0}
    return values.reduce(0) { (res, value) -> Double in
        let val = value-mean
        let square = val*val
        return res+square
    }
}

func calculateSumOfProductOfXMinusXMeanAndYMinusYMean(xs:[Double], ys:[Double], xMean:Double, yMean:Double) -> Double {
    guard xs.count > 0 && xs.count == ys.count else {return 0}
    var productArray:[Double] = []
    for i in 0..<xs.count {
        let product:Double = (xs[i]-xMean)*(ys[i]-yMean)
        productArray.append(product)
    }
    return productArray.reduce(0, +)
}

func calculateIntercept(slope:Double, xMean:Double, yMean:Double) -> Double {
    return yMean-slope*xMean
}


// MARK:- least square method

func linearRegression(_ xs: [Double], _ ys: [Double]) -> (Double) -> Double {
    let xMean = calculateMean(xs)
    let yMean = calculateMean(ys)
    let summationOfXMeanMinusXsValues = calculateSumOfSquareOfMeanMinusValues(values: xs, mean: xMean)
    let summationOfProductOfXMinusXMeanAndYMinusYMean = calculateSumOfProductOfXMinusXMeanAndYMinusYMean(xs: xs, ys: ys, xMean: xMean, yMean: yMean)
    let slope = summationOfProductOfXMinusXMeanAndYMinusYMean/summationOfXMeanMinusXsValues
    let intercept = calculateIntercept(slope: slope, xMean:xMean, yMean: yMean)
    return {x in intercept+slope*x}
}

let b = linearRegression([10, 8, 3, 3, 2, 1], [500, 400, 7000, 8500, 11000, 10500])
let y = b(4)

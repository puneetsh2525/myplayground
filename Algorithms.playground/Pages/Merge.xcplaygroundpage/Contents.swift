//: [Previous](@previous)
import Foundation

public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init(_ val: Int) {
        self.val = val
        self.next = nil
    }
}
/*
 Input: 4->2->1->3
 Output: 1->2->3->4
 */
class Solution {
    
    func merge(_ a:ListNode?, _ b:ListNode?) ->ListNode? {
        var a:ListNode? = a
        var b:ListNode? = b
        let dummy:ListNode = ListNode.init(0)
        var curr:ListNode? = dummy
        while a != nil && b != nil {
            if a!.val <= b!.val {
                curr?.next = a
                a = a?.next
            } else {
                curr?.next = b
                b = b?.next
            }
            curr = curr?.next
        }
        if a != nil {
            curr?.next = a
        }
        if b != nil {
            curr?.next = b
        }
        return dummy.next
    }
    
    func sortList(_ head: ListNode?) -> ListNode? {
        guard head != nil && head?.next != nil else {return head}
        var pre: ListNode? = nil
        var slow: ListNode? = head
        var fast: ListNode? = head
        while fast != nil && fast?.next != nil {
            pre = slow
            slow = slow?.next
            fast = fast?.next?.next
        }
        pre?.next = nil
        let l1 = sortList(head)
        let l2 = sortList(slow)
        return merge(l1, l2)
    }
}

func printNode(_ node:ListNode?) {
    var tempNode = node
    while(tempNode != nil) {
        print("\(tempNode!.val)->")
        tempNode = tempNode?.next
    }
}

let one:ListNode = ListNode.init(1)
let four:ListNode = ListNode.init(4)
let three:ListNode = ListNode.init(3)
let two:ListNode = ListNode.init(2)
let six:ListNode = ListNode.init(6)

four.next = two
two.next = one
one.next = three

let s = Solution()
let l = s.sortList(four)
printNode(l)

//: [Previous](@previous)

import Foundation

class Solution {
    func groupAnagrams(_ strs: [String]) -> [[String]] {
        var map:[String:[String]] = [String:[String]]()
        for str in strs {
            let sortedStr = String(str.sorted())
            if let value = map[sortedStr] {
                var array = value
                array.append(str)
                map[sortedStr] = array
            } else {
                map[sortedStr] = [str]
            }
        }
        var result = [[String]]()
        for (_, value) in map {
            result.append(value)
        }
        return result
    }
}

let s = Solution()
let g = s.groupAnagrams(["eat","tea","tan","ate","nat","bat"])

//[["sol"],["wow"],["gap"],["hem"],["yap"],["bum"],["ugh","ugh"],["aha"],["jab"],["eve"],["bop"],["lyx"],["jed"],["iva"],["rod"],["boo"],["brr"],["hog"],["nay"],["mir"],["deb","deb"],["aft"],["dis"],["yea"],["hos"],["rye"],["hey"],["doc"],["bob"],["eel"],["pen"],["job"],["max"],["oho"],["lye"],["ram"],["nap"],["elf"],["qua"],["pup","pup"],["let"],["gym"],["nam"],["bye"],["lon"]]

print(g)

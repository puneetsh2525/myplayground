//: [Previous](@previous)

import Foundation

func montyHallProblem() -> Bool {
    let prizeDoor = Int.random(in:1...3)
    
    let chooseDoor = Int.random(in:1...3)
    
    var openDoor:Int = -1
    repeat {
        openDoor = Int.random(in:1...3)
    } while openDoor == chooseDoor || openDoor == prizeDoor
    
    var changeMind:Int = -1
    repeat {
        changeMind = Int.random(in:1...3)
    } while changeMind == chooseDoor || changeMind == openDoor
    
    return changeMind == prizeDoor ? true : false
}

var winOriginalChoice = 0
var winChangedMind = 0
for _ in 0..<1000 {
    if montyHallProblem() {
        winChangedMind += 1
    } else {
        winOriginalChoice += 1
    }
}

print(winOriginalChoice)
print(winChangedMind)



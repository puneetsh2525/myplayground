//: [Previous](@previous)

import Foundation

extension String {
    
    func skipTable(for pattern:String) -> [Character:Int]? {
        var map:[Character:Int] = [:]
        let count = pattern.count
        for (i, c) in pattern.enumerated() {
            map[c] = count-i-1
        }
        return map.isEmpty ? nil : map
    }
    
    func backwardsSearch(for pattern:String, startIndex:Int) -> (Bool, Character?) {
        var endIndex = pattern.count-1
        let startI = self.index(self.startIndex, offsetBy: startIndex)
        guard startIndex+endIndex < self.count else {
            return (false, nil)
        }
        
        while endIndex >= 0 {
            let j = pattern.index(pattern.startIndex, offsetBy: endIndex)
            let i = self.index(startI, offsetBy: endIndex)
            if pattern[j] != self[i] {
                return (false, self[i])
            }
            endIndex -= 1
        }
        return (true, nil)
    }
    
    func index(of pattern: String) -> Int? {
        guard self.count > 0 && pattern.count > 0 && self.count >= pattern.count else {return nil}
        guard let skipTable = skipTable(for: pattern) else {return nil}
        var startIndex = 0
        while startIndex < self.count {
            let arg = backwardsSearch(for: pattern, startIndex: startIndex)
            guard arg.0 == false else {
                return startIndex
            }
            guard let c = arg.1 else {return nil}
            let value = skipTable[c] ?? 1
            startIndex += value
        }
        return nil
    }
}

let s = "asdsWsorld"
let index = s.index(of: "World")


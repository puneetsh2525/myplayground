//: [Previous](@previous)

import Foundation

// Letter Combinations of a Phone Number
class Solution {
    let map:[String:[String]]
    
    init() {
        var m:[String:[String]] = [:]
        m["2"] = ["a", "b", "c"]
        m["3"] = ["d", "e", "f"]
        m["4"] = ["g", "h", "i"]
        m["5"] = ["j", "k", "l"]
        m["6"] = ["m", "n", "o"]
        m["7"] = ["p", "q", "r", "s"]
        m["8"] = ["t", "u", "v"]
        m["9"] = ["w", "x", "y", "z"]
        map = m
    }
    
    func getCombs(_ a:[String], _ b:[String]) -> [String] {
        var result:[String] = []
        for i in a {
            for j in b {
                result.append(i+j)
            }
        }
        return result
    }
    
    func letterCombinations(_ digits: String) -> [String] {
        let stringArray = digits.map{String($0)}
        guard stringArray.count > 1 else {
            if stringArray.count == 0 {
                return []
            }else {
                return map[stringArray[0]]!
            }
        }
        var combs = getCombs(map[stringArray[0]] ?? [], map[stringArray[1]] ?? [])
        for i in 2..<stringArray.count {
            combs = getCombs(combs, map[stringArray[i]] ?? [])
        }
        return combs
    }
}

// Given a collection of distinct integers, return all possible permutations
extension Solution {
    func permute(_ nums: [Int]) -> [[Int]] {
        var result: [[Int]] = []
        var candidates: [Int] = []
        backtracking(&result, &candidates, nums, nums.count)
        return result
    }
    
    func backtracking(_ result: inout [[Int]], _ candidates: inout [Int], _ nums:[Int], _ length: Int) {
        guard candidates.count != length else {
            result.append(candidates)
            return
        }
        
        for i in 0..<nums.count {
            var numsCopy = nums
            numsCopy.remove(at: i)
            candidates.append(nums[i])
            backtracking(&result, &candidates, numsCopy, length)
            candidates.removeLast()
        }
    }
}

extension Solution {
    func generateParenthesis(_ n: Int) -> [String] {
        guard n > 0 else {
            return []
        }
        var result:[String] = []
        generateParenthesisHelper(result: &result, string:"", open: 0, close: 0, n: n)
        return result
    }
    
    func generateParenthesisHelper(result:inout [String], string:String, open:Int, close:Int, n:Int) {
        guard string.count < n*2 else {
            result.append(string)
            return
        }
        if open < n {
            generateParenthesisHelper(result: &result, string: string+"(", open: open+1, close: close, n: n)
        }
        if close < open {
            generateParenthesisHelper(result: &result, string: string+")", open: open, close: close+1, n: n)
        }
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}

extension Solution {
    struct BoardPosition {
        let char:Character
        var isVisited:Bool = false
    }
    
    func exist(_ board: [[Character]], _ word: String) -> Bool {
        guard word.count > 0 else {return false}
        var positionBoard:[[BoardPosition]] = []
        for array in board {
            let result = array.map{BoardPosition.init(char: $0, isVisited: false)}
            positionBoard.append(result)
        }
        for (i, array) in positionBoard.enumerated() {
            for (j,pos) in array.enumerated() where pos.isVisited == false && pos.char == word.first! {
                if checkForCharacterAtIndex(0, in: word, row: i, col: j, board: &positionBoard) {
                     return true
                }
            }
        }
        return false
    }
    
    func checkForCharacterAtIndex(_ index:Int, in word:String, row:Int, col:Int, board:inout [[BoardPosition]]) -> Bool {
        guard board[row][col].isVisited == false else {
            return false
        }
        guard index < word.count else {return false}
        let character = word[index]
        let boardChar = board[row][col].char
        board[row][col].isVisited = true
        guard character == boardChar else {
            return false
        }
        //print("character:\(character), row:\(row), col:\(col)")
        if index+1 == word.count {
            return true
        }
        var a = false
        var b = false
        var c = false
        var d = false
        if row+1 < board.count {
            a = checkForCharacterAtIndex(index+1, in: word, row: row+1, col: col, board: &board)
        }
        if col+1 < board[row].count {
            b = checkForCharacterAtIndex(index+1, in: word, row: row, col: col+1, board: &board)
        }
        if row-1 >= 0  {
            c = checkForCharacterAtIndex(index+1, in: word, row: row-1, col: col, board: &board)
        }
        if col-1 >= 0 {
            d = checkForCharacterAtIndex(index+1, in: word, row: row, col: col-1, board: &board)
        }
        let result = a || b || c || d
        board[row][col].isVisited = false
        return result
    }
}

let s = Solution()
s.exist(
    [["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a"],["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","b"]],
    "baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")

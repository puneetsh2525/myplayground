import Foundation

public class LinkedList<KeyType:Hashable>:CustomStringConvertible {
    public class ListNode<KeyType:Hashable> {
        public let data:KeyType
        public var prevNode:ListNode?
        public var nextNode:ListNode?
        public init(_ data:KeyType) {
            self.data = data
        }
    }
    
    private var keyNodeMap:[KeyType:ListNode<KeyType>] = [:]
    private let maxSize:Int
    private var head:ListNode<KeyType>?
    private var tail:ListNode<KeyType>?
    
    public init(_ maxSize:Int) {
        self.maxSize = maxSize
    }
    
    public func add(key:KeyType) -> KeyType? {
        let node = keyNodeMap[key] ?? ListNode.init(key)
        remove(key: key)
        add(node: node)
        keyNodeMap[key] = node
        if let cTail = tail, keyNodeMap.count > self.maxSize {
            remove(key: cTail.data)
            return cTail.data
        }
        return nil
    }
    
    private func remove(key:KeyType) {
        guard let node = keyNodeMap[key] else {
            return
        }
        keyNodeMap[key] = nil
        remove(node: node)
    }
    
    private func remove(node:ListNode<KeyType>) {
        if let cHead = head, cHead === node {
            head = cHead.nextNode
            head?.prevNode = nil
        } else if let cTail = tail, cTail === node {
            tail = cTail.prevNode
            tail?.nextNode = nil
        } else {
            let prev = node.prevNode
            let next = node.nextNode
            prev?.nextNode = next
            next?.prevNode = prev
        }
    }
    
    private func add(node:ListNode<KeyType>) {
        let nextNode = head
        node.nextNode = nextNode
        node.prevNode = nil
        nextNode?.prevNode = node
        head = node
        if head?.nextNode == nil {
            tail = head
        }
    }
    
    public var description: String {
        var string = ""
        var x = head
        var count = 1
        while x != nil {
            string += (x!.data as? String) ?? count.description 
            string += "->"
            x = x?.nextNode
            count += 1
        }
        return string
    }
}



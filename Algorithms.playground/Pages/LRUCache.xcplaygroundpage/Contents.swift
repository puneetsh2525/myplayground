//: [Previous](@previous)

import Foundation

public class LRUCache<KeyType:Hashable>:CustomStringConvertible {
    
    private let maxSize:Int
    private var cache:[KeyType:Any] = [:]
    private let queue:LinkedList<KeyType> // Uses Doubly Linked List
    
    public init(_ maxSize:Int) {
        self.maxSize = maxSize
        self.queue = LinkedList<KeyType>(maxSize)
    }
    
    public func insert(object:Any, for key:KeyType) {
        cache[key] = object
        addKeyInQueue(key: key)
    }
    
    public func getObject(for key:KeyType) -> Any? {
        addKeyInQueue(key: key)
        return cache[key]
    }
    
    private func addKeyInQueue(key:KeyType) {
        if let removeKey = queue.add(key: key) {
            cache[removeKey] = nil
        }
    }
    
    public var description:String {
        return queue.description
    }
}


/////////// TESTS


let cache = LRUCache<String>(3)
cache.insert(object: 1, for: "1")
cache.insert(object: 2, for: "2")
cache.insert(object: 3, for: "3")
cache.insert(object: 4, for: "4")
cache.getObject(for: "1")


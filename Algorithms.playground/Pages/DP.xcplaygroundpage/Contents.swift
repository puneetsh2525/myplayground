//: [Previous](@previous)

import Foundation

// Climbing Stairs
class Solution {
    var map:[Int:Int] = [:]
    func climbStairs(_ n: Int) -> Int {
        guard n > 0 else {return 0}
        if n == 1 {
            map[n] = 1
            return 1
        } else if n == 2 {
            map[n] = 2
            return 2
        } else {
            if let value = map[n] {
                return value
            }
            else {
                let value = climbStairs(n-1) + climbStairs(n-2)
                map[n] = value
                return value
            }
        }
    }
}

// Best Time to Buy and Sell Stock
extension Solution {
    func maxProfit(_ prices: [Int]) -> Int {
        guard prices.count > 0 else {return 0}
        var bi = 0
        var maxProfit = 0
        for i in 1..<prices.count {
            if prices[bi] >= prices[i] {
                bi = i
            } else {
                let profit = prices[i]-prices[bi]
                maxProfit = max(profit, maxProfit)
            }
        }
        return maxProfit
    }
}

// Maximum Subarray
extension Solution {
    func maxSubArray(_ nums: [Int]) -> Int {
        guard nums.count > 0 else {return 0}
        var maxSoFar = nums.first ?? 0
        var maxEnding = nums.first ?? 0
        for i in 1..<nums.count {
            let num = nums[i]
            maxEnding += num
            maxEnding = num > maxEnding ? num : maxEnding
            maxSoFar = maxSoFar<maxEnding ? maxEnding : maxSoFar
        }
        return maxSoFar
    }
}

// HouseRobber
extension Solution {
    func rob(_ nums: [Int]) -> Int {
        guard nums.count > 0 else {return 0}
        var prev1 = 0
        var prev2 = 0
        for num in nums {
            let temp = prev1
            prev1 = max(prev2+num, prev1)
            prev2 = temp
        }
        return prev1
    }
}

let s = Solution()
let num = [-2, 1]
s.maxSubArray(num)



//: [Previous](@previous)

import Foundation

func isPalindrome(input:String) -> Bool {
    guard input.count > 1 else {
        return true
    }
    
    var startIndex = input.startIndex
    var endIndex = input.index(before: input.endIndex)
    let validCharSet = Set<Character>.init(arrayLiteral: "a","b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
    
    while startIndex <= endIndex {
        let x = Character(String(input[startIndex]).lowercased())
        let y = Character(String(input[endIndex]).lowercased())
       
        guard validCharSet.contains(x) else {
            startIndex = input.index(after: startIndex)
            continue
        }
        
        guard validCharSet.contains(y) else {
            endIndex = input.index(before: endIndex)
            continue
        }
        
        guard x != y else {
            startIndex = input.index(after: startIndex)
            endIndex = input.index(before: endIndex)
            continue
        }
        
        return false
    }
    return true
}

isPalindrome(input: "Radars")

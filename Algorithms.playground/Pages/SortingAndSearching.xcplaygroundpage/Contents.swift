//: [Previous](@previous)

import Foundation

// Merge Sorted Array
class Solution {
    func merge(_ nums1: inout [Int], _ m: Int, _ nums2: [Int], _ n: Int) {
        var i = 0, j = 0, k = 0
        let nums1Temp = nums1
        while i < m && j < n {
            if nums1Temp[i] <= nums2[j] {
                nums1[k] = nums1Temp[i]
                i += 1
            } else {
                nums1[k] = nums2[j]
                j += 1
            }
            k += 1
        }
        while i < m {
            nums1[k] = nums1Temp[i]
            i += 1
            k += 1
        }
        
        while j < n {
            nums1[k] = nums2[j]
            j += 1
            k += 1
        }
    }
}

// MARK:- Sort Colors
extension Solution {
    func sortColors(_ nums: inout [Int]) {
        guard nums.count > 1 else {return}
        var i = 0
        var j = nums.count-1
        var k = 0
        while k >= i && k <= j  {
            let num = nums[k]
            switch num {
            case 0:
                nums.swapAt(i, k)
                i += 1
                k = i
            case 1:
                k += 1
            case 2:
                nums.swapAt(k, j)
                j -= 1
            default:
                break
            }
        }
    }
}

// MARK:- Top K Frequent Elements
extension Solution {
    func findLargestValueKeyIn(_ dict:inout[Int:Int]) -> Int {
        var largest = (0, 0)
        for arg in dict {
            if arg.value > largest.1 {
                largest = arg
            }
        }
        dict[largest.0] = nil
        return largest.0
    }
    
    func topKFrequent(_ nums: [Int], _ k: Int) -> [Int] {
        var map:[Int:Int] = [:]
        for num in nums {
            map[num] = (map[num] ?? 0)+1
        }
        var result:[Int] = []
        var count = 0
        let sortedArray = map.sorted(by: {$0.value > $1.value})
        for arg in sortedArray {
            if count >= k {break}
            result.append(arg.key)
            count += 1
        }
        return result
    }
}

// MARK:- Kth Largest Element in an Array
extension Solution {
    
    func partition(nums:inout [Int], p:Int, r:Int) -> Int {
        let x = nums[r]
        var i = p-1
        for j in p..<r {
            if nums[j] <= x {
                i = i+1
                nums.swapAt(i, j)
            }
        }
        i = i+1
        nums.swapAt(i, r)
        return i
    }
    
    func quickSortHelper(nums:inout [Int], p:Int, r:Int, k:Int, result:inout Int) {
        guard p < r  else {
            return
        }
        let q = partition(nums: &nums, p: p, r: r)
        if q == k {
            result = nums[q]
        } else {
            quickSortHelper(nums: &nums, p: p, r: q-1, k: k, result: &result)
            quickSortHelper(nums: &nums, p: q+1, r: r, k: k, result: &result)
        }
    }
    
    func findKthLargest(_ nums: [Int], _ k: Int) -> Int {
        var result = -1
        var nums = nums
        quickSortHelper(nums: &nums, p: 0, r: nums.count-1, k: nums.count-k, result: &result)
        if result == -1 {
            result = nums[nums.count-k]
        }
        return result
    }
}

// MARK:- Find Peak Element
extension Solution {
    func findPeakElement(_ nums: [Int]) -> Int {
        guard nums.count > 0 else {
            return -1
        }
        var left = 0
        var right = nums.count-1
        var mid = nums.count/2
        
        guard left != right else {
            return 0
        }
        
        while(true) {
            if left+1 == right {
                return nums[left] < nums[right] ? right : left
            }
            mid = left + (right-left)/2
            if nums[mid] < nums[mid+1] {
                left = mid
            } else {
                right = mid
            }
        }
    }
}

extension Solution {
    func searchStartEndFor(_ nums:[Int], left:Int, right:Int, mid:Int) {
      
        
    }
    
    func searchRange(_ nums: [Int], _ target: Int) -> [Int] {
        
        guard nums.count > 0 else {
            return [-1, -1]
        }
        
        var left = 0
        var right = nums.count-1
        while true {
            if left > right {
                return [-1, -1]
            }
            let mid = left + (right-left)/2
            
            if nums[mid] > target {
                right = mid-1
            } else if nums[mid] < target {
                left = mid+1
            } else {
                // search linear
                var start = mid
                var end = mid
                
                var tempStart = start-1
                var tempEnd = end+1
                
                while left <= tempStart {
                    if nums[tempStart] == target {
                        start = tempStart
                    }
                    tempStart -= 1
                }
                while tempEnd <= right {
                    if nums[tempEnd] == target {
                        end = tempEnd
                    }
                    tempEnd += 1
                }
                return [start, end]
            }
        }
    }
}

// MARK:- Merge Intervals
extension Solution {
    public class Interval:CustomStringConvertible {
        public var start: Int
        public var end: Int
        public init(_ start: Int, _ end: Int) {
            self.start = start
            self.end = end
        }
        var description: String {
            return "(start:\(start), end:\(end))"
        }
    }
    
    func merge(_ intervals: [Interval]) -> [Interval] {
        guard intervals.count > 0 else {return []}
        let sortedIntervals = intervals.sorted(by: {$0.start < $1.start})
        var result = [Interval]()
        result.append(sortedIntervals[0])
        
        for i in 1..<sortedIntervals.count {
            let last = result.popLast()!
            result.append(contentsOf:mergeHelper(last, B: sortedIntervals[i]))
        }
        return result
    }
    
    private func mergeHelper(_ A:Interval, B:Interval) -> [Interval] {
        if A.start <= B.start && A.end >= B.end {
            return [A]
        } else if B.start <= A.start && B.end >= A.end {
            return [B]
        } else if A.start < B.start && A.end < B.start {
            return [A, B]
        } else if B.start < A.start && B.end < A.start {
            return [B, A]
        } else if A.start <= B.start && A.end < B.end {
            return [Interval.init(A.start, B.end)]
        } else if B.start <= A.start && B.end < A.end {
            return [Interval.init(B.start, A.end)]
        } else {
            return [A, B]
        }
    }
}

// MARK:- Search in Rotated Sorted Array
extension Solution {
    func findIndexWhereArrayIsRotated(left:Int, right:Int, nums:[Int]) -> Int? {
        guard left+1 != right else {
            return nums[left] > nums[right] ? right : nil
        }
        let mid = left + (right-left)/2
        if nums[mid] < nums[left] {
            return findIndexWhereArrayIsRotated(left:left, right:mid, nums:nums)
        } else {
            return findIndexWhereArrayIsRotated(left:mid, right:right, nums:nums)
        }
    }
    
    func binarySearch(_ nums:[Int], left:Int, right:Int, target:Int) -> Int {
        var left = left
        var right = right
        while left <= right {
            let mid = left + (right-left)/2
            let num = nums[mid]
            if target == num {
                return mid
            } else if target > num {
                left = mid+1
            } else {
                right = mid-1
            }
        }
        return -1
    }
    
    func search(_ nums: [Int], _ target: Int) -> Int {
        guard nums.count > 1 else {
            let first = nums.first ?? Int.min
            return first == target ? 0 : -1
        }
        if let index = findIndexWhereArrayIsRotated(left: 0, right: nums.count-1, nums: nums) {
            let a = binarySearch(nums, left: index, right: nums.count-1, target: target)
            let b = binarySearch(nums, left: 0, right: index-1, target: target)
            return a != -1 ? a : b
        } else {
            return binarySearch(nums, left: 0, right: nums.count-1, target: target)
        }
    }
}

// MARK:- Search a 2D Matrix II
extension Solution {
    func searchMatrix(_ matrix: [[Int]], _ target: Int) -> Bool {
        for array in matrix {
            if let last = array.last, let first = array.first, first <= target && last >= target {
                let binarySearchResult = binarySearch(array, left: 0, right: array.count-1, target: target)
                if binarySearchResult != -1 {
                    return true
                } else {
                    continue
                }
            } else {
                continue
            }
        }
        return false
    }
}

// MARK:- Wiggle Sort II
extension Solution {
    func wiggleSort(_ nums: inout [Int]) {
        let count = nums.count
        guard count > 1 else {
            return
        }
        let m = (count+1)/2
        let median = findKthLargest(nums, m)
        var i = 0, left = 0, right = count-1
        while left <= right {
            if nums[index(i: left, n: count)] > median {
                nums.swapAt(index(i: i, n: count), index(i: left, n: count))
                i += 1
                left += 1
            } else if nums[index(i: left, n: count)] < median {
                nums.swapAt(index(i: left, n: count), index(i: right, n: count))
                right -= 1
            } else {
                left += 1
            }
        }
    }
    
    private func index(i:Int, n:Int) -> Int {
        return (2 * i + 1) % (n | 1)
    }
}

// Kth Smallest Element in a Sorted Matrix

extension Solution {
    func kthSmallest(_ matrix: [[Int]], _ k: Int) -> Int {
        var map = matrix.flatMap{$0}
        map.sort()
        return map[k-1]
    }
}

// Median of Two Sorted Arrays

//extension Solution {
//    func findMedian(_ nums:[Int]) -> Double {
//        if nums.count%2 == 1 {
//            return Double(nums[nums.count/2])
//        } else {
//            return Double((nums[nums.count/2] + nums[nums.count/2-1]))/2
//        }
//    }
//
//    func findMedianSortedArrays(_ nums1: [Int], _ nums2: [Int]) -> Double {
//        guard nums1.count > 0 else {
//            return findMedian(nums2)
//        }
//        guard nums2.count > 0 else {
//            return findMedian(nums1)
//        }
//        var i = nums1.count
//    }
//}

let s = Solution()
var nums = [
    [ 1,  5,  9],
    [10, 11, 13],
    [12, 13, 15]
]

s.kthSmallest(nums, 8)

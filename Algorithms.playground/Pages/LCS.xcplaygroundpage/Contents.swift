//: [Previous](@previous)

import Foundation

extension String {
    public func longestCommonSubsequence(_ other: String) -> String {
        
        func lcsLength(_ other: String) -> [[Int]] {
            var lengthArray:[[Int]] = [[Int]].init(repeating: [Int].init(repeating: 0, count: other.count+1), count: self.count+1)
            for (i, selfChar) in self.enumerated() {
                for (j, otherChar) in other.enumerated() {
                    if selfChar == otherChar {
                        lengthArray[i+1][j+1] = lengthArray[i][j]+1
                    } else {
                        lengthArray[i+1][j+1] = max(lengthArray[i][j+1], lengthArray[i+1][j])
                    }
                }
            }
            return lengthArray
        }
        
        func backtrack(_ matrix: [[Int]]) -> String {
            
            var i = count
            var j = other.count
            var c = endIndex
            var lcs = String()
            while i > 0 && j > 0 {
                if matrix[i][j] == matrix[i][j-1] {
                    j -= 1
                } else if matrix[i][j] == matrix[i-1][j] {
                    i -= 1
                    c = index(before: endIndex)
                } else {
                    i -= 1
                    j -= 1
                    c = index(before: c)
                    lcs.append(self[c])
                }
            }
            return String(lcs.reversed())
        }
        
        return backtrack(lcsLength(other))
    }
}


"Hello World".longestCommonSubsequence("Bonjour le monde")   // "oorld"

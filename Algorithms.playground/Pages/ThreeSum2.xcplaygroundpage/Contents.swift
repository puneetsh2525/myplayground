//: [Previous](@previous)

import Foundation

class Solution {
    func getAfterValue(_ nums:[Int], val:Int) -> Int {
        var newVal = val+1
        while newVal < nums.count && nums[newVal] == nums[val] {
            newVal = newVal+1
        }
        return newVal
    }
    
    func getBeforeValue(_ nums:[Int], val:Int) -> Int {
        var newVal = val-1
        while newVal >= 0 && nums[newVal] == nums[val] {
            newVal = newVal-1
        }
        return newVal
    }
    
    func threeSum(_ nums: [Int]) -> [[Int]] {
        var nums = nums.sorted()
        var result = [[Int]]()
        var l = 0
        while l < nums.count {
            var m = l+1
            var r = nums.count-1
            while m < r {
                let sum = nums[l]+nums[m]+nums[r]
                if sum == 0 {
                    result.append([nums[l], nums[m], nums[r]])
                    m = getAfterValue(nums, val: m)
                    r = getBeforeValue(nums, val: r)
                } else if sum < 0 {
                    m = getAfterValue(nums, val: m)
                } else {
                    r = getBeforeValue(nums, val: r)
                }
            }
            l = getAfterValue(nums, val: l)
        }
        return result
    }
}

let s = Solution()
let a = [0, 0, 0]
s.threeSum(a)



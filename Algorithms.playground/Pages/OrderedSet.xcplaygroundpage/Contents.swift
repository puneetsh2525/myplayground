//: [Previous](@previous)

import Foundation
import UIKit

public class OrderedSet<T: Hashable> {
    
    var set:Set<T> = Set<T>()
    var array:[T] = [T]()
    
    public func add(_ object: T) {
        guard set.contains(object) == false else {return}
        set.insert(object)
        array.append(object)
    }
    
    public func insert(_ object: T, at index: Int) {
        guard set.contains(object) == false else {return}
        set.insert(object)
        array.insert(object, at:index)
    }
    
    public func object(at index: Int) -> T {
        return array[index]
    }
    
    public func set(_ object: T, at index: Int) {
        guard set.contains(object) == false else {return}
        let element = array[index]
        set.remove(element)
        array[index] = object
    }
    
    public func indexOf(_ object: T) -> Int {
        for (i, element) in array.enumerated() {
            if element == object {
                return i
            }
        }
        return -1
    }
    
    public func remove(_ object: T) {
        guard set.contains(object) == false else {return}
        let index =  indexOf(object)
        guard index != -1 else {return}
        set.remove(object)
        array.remove(at:index)
    }
    
}

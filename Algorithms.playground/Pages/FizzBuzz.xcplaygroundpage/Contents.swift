//: [Previous](@previous)

import Foundation

func fizzBuzz(_ numberOfTurns: Int) {
    
    for i in 1...numberOfTurns {
        var result = ""
        var terminator = ""
        if i%3 == 0 {
            result = "Buzz, "
        }
        
        if i%5 == 0 {
            result += "Fizz"
            terminator = ", "
        }
        if result.isEmpty {
            result = "\(i)"
            terminator = ", "
        }
        if i == numberOfTurns {
            terminator = ""
        }
        print(result, separator: "", terminator: terminator)
    }
    
}

fizzBuzz(15)

//: [Previous](@previous)

import Foundation


func prefixArrayFor(pattern:String) -> [Int]? {
    guard pattern.count > 0 else {return nil}
    var array = [Int](repeating: 0, count: pattern.count)
    var j = 0
    var i = 1
    while i < array.count {
        let jIndex = pattern.index(pattern.startIndex, offsetBy: j)
        let iIndex = pattern.index(pattern.startIndex, offsetBy: i)
        if pattern[jIndex] == pattern[iIndex] {
            array[i] = j+1
            j += 1
            i += 1
        } else if j > 0 {
            j = array[j-1]
        } else {
            i += 1
        }
    }
    return array
}

extension String {
    func indexesOf(ptnr: String) -> [Int]? {
        guard let prefixArray = prefixArrayFor(pattern: ptnr) else {return nil}
        guard count > 0 else {return nil}
        
        var result:[Int] = []
        var stringI = 0
        
        var pttrnI:Int!
        var patternMatchLength:Int!
        var strtIndex:Int!
        
        let reset:()->() = {
            pttrnI = 0
            patternMatchLength = 0
            strtIndex = stringI
        }
        reset()
        
        while stringI < count {
            let pttrnI2 = ptnr.index(ptnr.startIndex, offsetBy: pttrnI)
            let stringI2 = self.index(self.startIndex, offsetBy: stringI)
            if ptnr[pttrnI2] == self[stringI2] {
                if pttrnI == ptnr.count-1 {
                    result.append(strtIndex)
                    stringI += 1
                    
                    reset()
                } else {
                    stringI += 1
                    pttrnI += 1
                    patternMatchLength += 1
                }
            } else {
                stringI = strtIndex
                
                let tableMatch = patternMatchLength > 0 ? prefixArray[patternMatchLength-1] : prefixArray[0]
                let inc = tableMatch > 1 ? patternMatchLength-tableMatch : 1
                stringI += inc
                
                reset()
            }
        }
        return result.isEmpty ? nil : result
    }
}

let p = "ACTGACTA"
let s = "GCACTGACTGACTGACTAG"
s.indexesOf(ptnr: p)

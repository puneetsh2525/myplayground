This repository is created for following reasons:

- To maintain code that helps in answering StackOverFlow problems.
- To learn data structures (refer:https://github.com/raywenderlich/swift-algorithm-club)
- To solve common algorithmic problems
- To learn new things in Swift

### StackOverFlow

- AppearanceInSegmentedControl

### Data Structures

- Union-Find
- [Heap](https://bitbucket.org/puneetsh2525/myplayground/src/db1c859d833810e9efe5d801d44be7fa9733c317/PriorityQueue/PriorityQueue.playground/Sources/Heap.swift?at=master&fileviewer=file-view-default)

### Problems

- QuickSort
- MinMaxInArray
- LinearSearch in array
- kth Largest Element In Array
- Fibonacci Numbers
- Count Occurences
- Boyer-Moore
- Binary Search

### Swift Concepts
- Custom Operators


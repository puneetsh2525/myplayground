// Count occurences of key in sorted array

import Foundation

// Goal: Count how often a certain value appears in an array.

func countOccurrencesOfKey(_ key:Int, inArray array:Array<Int>) -> Int {
    
    func lowerBoundary() -> Int {
        var low = 0
        var high = array.count
        while low < high {
            let mid = low + (high-low)/2
            if array[mid] < key  {
                low = mid+1
            } else {
                high = mid
            }
        }
        return low
    }
    
    func higherBoundary() -> Int {
        var low = 0
        var high = array.count
        while low < high {
            let mid = low + (high-low)/2
            if array[mid] > key  {
                high = mid
            } else {
                low = mid+1
            }
        }
        return high
    }
    
    return higherBoundary()-lowerBoundary()
    
}
let a = [ 0, 1, 1, 3, 3, 3, 3, 6, 8, 10, 11, 11 ]

countOccurrencesOfKey(3, inArray: a)

// Goal: To use quicksort to sort an array using random pivot and dutch national flag partition scheme

import Foundation

extension Array where Element:Comparable {
    func quickSort() -> Array {
        guard self.count > 1 else {
            return self
        }
        var a = self
        quickSortHelper(a: &a, low: 0, high: a.count-1)
        return a
    }
    
    
    private func random(min:Int, max:Int) -> Int {
        let upperbound = UInt32(max) - UInt32(min)
        return Int(arc4random_uniform(upperbound)) + min
    }
    
    private func partitionDutchFlag<T:Comparable>(a: inout [T], low:Int, high:Int, pivotIndex:Int) -> (Int, Int) {
        let pivot = a[pivotIndex]
        var smaller = low
        var equal = low
        var larger = high
        
        while equal <= larger {
            if a[equal] < pivot {
                if equal != smaller {
                    a.swapAt(equal, smaller)
                }
                equal += 1
                smaller += 1
            } else if a[equal] == pivot {
                equal += 1
            } else {
                if equal != larger {
                    a.swapAt(equal, larger)
                }
                larger -= 1
            }
        }
        return (smaller, larger)
    }
    
    private func quickSortHelper<T:Comparable>(a: inout [T], low:Int, high:Int) {
        if low < high {
            let randomPivotIndex = random(min: low, max: high)
            let p = partitionDutchFlag(a: &a, low: low, high: high, pivotIndex: randomPivotIndex)
            quickSortHelper(a: &a, low: low, high: p.0-1)
            quickSortHelper(a: &a, low: p.1+1, high: high)
        }
    }
}


[1, 3, 2, 19, 6, 27, 21].quickSort()
["Apple", "Son", "Ball"].quickSort()
["3.14.10", "3.130.10"].quickSort() // to sort such an array    we need to use a quicksort version with a block
